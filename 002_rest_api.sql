-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Agu 2020 pada 06.42
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `002_rest_api`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_gifts`
--

CREATE TABLE `tbl_gifts` (
  `id_gift` int(11) NOT NULL,
  `name_gift` text NOT NULL,
  `point_gift` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_gifts`
--

INSERT INTO `tbl_gifts` (`id_gift`, `name_gift`, `point_gift`, `image`) VALUES
(6, 'Masker Korea', '2', '751952770_a520e79b-9ee5-44bc-b69b-25d978d1e7dc_800_800.jpg'),
(13, 'Kompor Gas ', '25', '32940930_b0b81004-abbf-4a59-a523-f9f015112711_543_543.jpg'),
(14, 'Sabun batang', '3', 'HTB1XuErXBLN8KJjSZFpq6zZaVXaH.jpg'),
(15, 'Sampo Rejoice', '6', '62981870_a233633b-f539-4882-ba64-ac441e7c3a3f_540_855.png'),
(16, 'Beras 25KG', '25', '32915789_a13371f1-32d0-4b8a-bd1e-b37b7726799c_2048_1828.jpg'),
(17, 'Lemari Top ', '40', '25648058_abb2baf3-f562-45de-b298-9405a558edfc_712_608.png'),
(18, 'Sandal Mael', '8', 'panama-4327-2250012-1.jpg'),
(19, 'Sepatu Leedo', '22', '5717073_3d33ff91-c99c-4097-9856-6310459d826b_750_750.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_point`
--

CREATE TABLE `tbl_point` (
  `id_user` int(11) NOT NULL,
  `point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_point`
--

INSERT INTO `tbl_point` (`id_user`, `point`) VALUES
(10, 34),
(11, 3),
(12, 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_products`
--

CREATE TABLE `tbl_products` (
  `id_product` int(11) NOT NULL,
  `name_product` text NOT NULL,
  `price_product` int(11) NOT NULL,
  `description_product` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_products`
--

INSERT INTO `tbl_products` (`id_product`, `name_product`, `price_product`, `description_product`, `image`) VALUES
(4, 'Lenovo Mag Pro', 10000000, 'Lenovo Mag Pro V1', '6t8Zh249QiFmVnkQdCCtHK.jpg'),
(5, 'Asus Vivo Book', 14500000, 'Asus Rilis Duo Laptop VivoBook dengan Layar Tipis', '5d7f156c36b281.png'),
(8, 'Lenovo First Gen 1', 3500000, 'Lenobo Gen1 Legend', 'A4GDK27VMnz6LtFDy9yzk.jpg'),
(10, 'Asus Beats Acer', 5000000, 'Asus Beats Acer Pro Black', 'VLS_2759.jpg'),
(11, 'Samsung', 3000000, 'Samsung A67', 'Pile-of-2019-flagship-phones-scaled.jpg'),
(13, 'Xiomi', 1900000, 'Xiomi S89', 'IMG_20191201_124400.jpg'),
(14, 'Luna', 1500000, 'Android Versi Terbaru', 'Simo-Luna-black-696x522.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksihadiah`
--

CREATE TABLE `tbl_transaksihadiah` (
  `id_transaksihadiah` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_hadiah` int(11) NOT NULL,
  `nama_hadiah` text NOT NULL,
  `point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_transaksihadiah`
--

INSERT INTO `tbl_transaksihadiah` (`id_transaksihadiah`, `id_user`, `id_hadiah`, `nama_hadiah`, `point`) VALUES
(1, 10, 6, 'Masker Korea', 2),
(2, 10, 6, 'Masker Korea', 2),
(3, 10, 6, 'Masker Korea', 2),
(4, 10, 6, 'Masker Korea', 2),
(5, 10, 6, 'Masker Korea', 2),
(6, 11, 6, 'Masker Korea', 2),
(7, 10, 18, 'Sandal Mael', 8),
(8, 10, 18, 'Sandal Mael', 8),
(9, 12, 14, 'Sabun batang', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksiproduk`
--

CREATE TABLE `tbl_transaksiproduk` (
  `id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `name_product` text NOT NULL,
  `price_product` int(11) NOT NULL,
  `qty_product` int(11) NOT NULL,
  `harga_total` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_transaksiproduk`
--

INSERT INTO `tbl_transaksiproduk` (`id_transaksi`, `id_user`, `id_product`, `name_product`, `price_product`, `qty_product`, `harga_total`, `status`) VALUES
(2, 10, 14, 'Luna', 1500000, 10, 15000000, 1),
(6, 10, 13, 'Xiomi', 1900000, 13, 24700000, 1),
(7, 10, 11, 'Samsung', 3000000, 2, 6000000, 1),
(8, 10, 14, 'Luna', 1500000, 23, 34500000, 1),
(9, 11, 12, 'Realme V5', 1888000, 10, 18880000, 1),
(10, 10, 12, 'Realme V5', 1888000, 3, 5664000, 1),
(11, 10, 12, 'Realme V5', 1888000, 10, 18880000, 1),
(12, 12, 13, 'Xiomi', 1900000, 3, 5700000, 1),
(13, 12, 11, 'Samsung', 3000000, 4, 12000000, 1),
(14, 12, 5, 'Asus Vivo Book', 14500000, 2, 29000000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id_user` int(11) NOT NULL,
  `username` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `level` enum('admin','user') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`id_user`, `username`, `email`, `password`, `level`) VALUES
(8, 'a', 'a@a.a', '$2y$10$coadiJzuT20loGaOGECL0.HulmWEvhLsc.zqtjpmr51ECQQ37Mdiu', 'admin'),
(10, 'b', 'b@b.b', '$2y$10$U3tZzzc.Pro/StD03AXugOhYpM9QOfBy9.euS03hEwz.O.kiyHWm6', 'user'),
(11, 'c', 'c@c.c', '$2y$10$4Ik0Mmd6spFTVS.na65c.uAClRmn4TnpTAn5EI5Tf43JABv9wMM5O', 'user'),
(12, 'diyar', 'diyar.codes@gmail.com', '$2y$10$bRbq5skxdYmGh3Q9ElyeyOECuigRYXi7WBFMrPnrDuYbnyoFSmzfO', 'user'),
(13, 'chus', 'chus@gmail.com', '$2y$10$FIhcSuujSULbSjj8PVDQPe4beRkaryS88kbtLS32jIFYFgdqqXsdK', 'user');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_gifts`
--
ALTER TABLE `tbl_gifts`
  ADD PRIMARY KEY (`id_gift`);

--
-- Indeks untuk tabel `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`id_product`);

--
-- Indeks untuk tabel `tbl_transaksihadiah`
--
ALTER TABLE `tbl_transaksihadiah`
  ADD PRIMARY KEY (`id_transaksihadiah`);

--
-- Indeks untuk tabel `tbl_transaksiproduk`
--
ALTER TABLE `tbl_transaksiproduk`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_gifts`
--
ALTER TABLE `tbl_gifts`
  MODIFY `id_gift` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tbl_transaksihadiah`
--
ALTER TABLE `tbl_transaksihadiah`
  MODIFY `id_transaksihadiah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tbl_transaksiproduk`
--
ALTER TABLE `tbl_transaksiproduk`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
