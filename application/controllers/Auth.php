<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use \Firebase\JWT\JWT;

class Auth extends RestController
{
    function index_get()
    {
        echo "this work";
    }

    function register_post()
    {
        $data = [
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'level' => 'user'
        ];

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        $this->form_validation->set_rules('passwordConfirm', 'Password Konfirmasi', 'required|matches[password]');

        if ($this->form_validation->run() == false) {
            if (form_error('username')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('username')
                ], 404);
            } else if (form_error('email')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('email')
                ], 404);
            } else if (form_error('password')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('password')
                ], 404);
            } else if (form_error('passwordConfirm')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('passwordConfirm')
                ], 404);
            }
        }

        if ($this->Auth_model->insertUser($data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'User Berhasil registrasi'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'User Gagal registrasi'
            ], 404);
        }
    }

    function login_post()
    {
        $data = [
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password')
        ];

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            if (form_error('email')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('email')
                ], 404);
            } else if (form_error('password')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('password')
                ], 404);
            }
        }

        $getUser = $this->Auth_model->getUserByEmail($data['email']);

        if (empty($getUser)) {
            $this->response([
                'status' => false,
                'message' => 'Email belum terdaftar'
            ], 404);
        }

        if (!password_verify($data['password'], $getUser['password'])) {
            $this->response([
                'status' => false,
                'message' => 'Password salah'
            ], 404);
        }

        $key = "example_key";
        $payload = array(
            "iss"      => "http://example.org",
            "aud"      => "http://example.com",
            "iat"      => 1356999524,
            "nbf"      => 1357000000,
            "id_user"  => $getUser['id_user'],
            "email"    => $getUser['email'],
            "username"    => $getUser['username'],
            "level" => $getUser['level']
        );

        $jwt = JWT::encode($payload, $key);

        $this->response([
            'status'  => true,
            'message' => 'Login sukses silahkan masukkan token',
            'token'   => $jwt,
            'data'    => $getUser
        ], 200);
    }
}
