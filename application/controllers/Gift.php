<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use \Firebase\JWT\JWT;

class Gift extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
    }

    function index_get()
    {
        $id = $this->get('id_gift');
        $gift = $this->Gift_model->getGift($id);
        if ($gift) {
            $this->response([
                'status' => true,
                'data' => $gift,
                'message' => 'Produk ditemukan'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $gift,
                'message' => 'Produk tidak ditemukan'
            ], 404);
        }
    }

    function uploadImage()
    {
        $config['upload_path']          = './assets/img/';
        $config['allowed_types']        = 'jpeg|jpg|png';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $this->response([
                'status'   => false,
                'message' => $this->upload->display_errors()
            ], 404);
        } else {
            return $this->upload->data('file_name');
        }
    }

    function index_post()
    {
        $data = [
            'name_gift'  => htmlspecialchars($this->post('name_gift', true)),
            'point_gift' => htmlspecialchars($this->post('point_gift', true)),
            'image'       => $this->uploadImage()
        ];

        $this->form_validation->set_rules('name_gift', 'Name Gift', 'required');
        $this->form_validation->set_rules('point_gift', 'Point Gift', 'required|numeric');

        if ($this->form_validation->run() == false) {
            unlink(FCPATH . '/assets/img/' . $this->upload->data('file_name'));
            if (form_error('name_gift')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('name_gift')
                ], 404);
            } else if (form_error('point_gift')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('point_gift')
                ], 404);
            }
        }

        if ($this->Gift_model->insertGift($data) == true) {
            $this->response([
                'status' => true,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        }
    }

    function index_delete()
    {
        $id = $this->delete('id_gift');

        if ($this->Gift_model->cekIdGift($id) == 0) {
            $this->response([
                'status' => false,
                'message' => 'id produk tidak ditemukan'
            ], 404);
        }

        $gambar = $this->Gift_model->getGift($id);

        if ($this->Gift_model->deleteGift($id) == true) {
            unlink(FCPATH . 'assets/img/' . $gambar->image);
            $this->response([
                'status' => true,
                'data' => $id,
                'message' => 'Data Berhasil dihapus'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data gagal dihapus'
            ], 404);
        }
    }

    function changeUploadImage($gambarLama)
    {
        $config['upload_path']          = './assets/img/';
        $config['allowed_types']        = 'jpeg|jpg|png';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            return $gambarLama;
        } else {
            unlink(FCPATH . '/assets/img/' . $gambarLama);
            return $this->upload->data('file_name');
        }
    }

    function index_put()
    {
        $id = $this->input->post('id_gift');

        $cekIdGhift = $this->Gift_model->getGift($id);
        $gambarLama = $cekIdGhift->image;

        $data = [
            'name_gift'  => htmlspecialchars($this->input->post('name_gift', true)),
            'point_gift' => htmlspecialchars($this->input->post('point_gift', true)),
            'image'       => $this->changeUploadImage($gambarLama)
        ];

        $this->form_validation->set_rules('name_gift', 'Name Gift', 'required');
        $this->form_validation->set_rules('point_gift', 'Point Gift', 'required|numeric');

        if ($this->form_validation->run() == false) {
            unlink(FCPATH . '/assets/img/' . $this->upload->data('file_name'));
            if (form_error('name_gift')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('name_gift')
                ], 404);
            } else if (form_error('point_gift')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('point_gift')
                ], 404);
            }
        }

        if ($this->Gift_model->updateGift($id, $data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'Data Berhasil diubah'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }
}
