<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Mygift extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
    }

    function index_get()
    {
        // $idUser = $this->validation->validationToken()->id_user;
        $id = $this->get('id_gift');

        $mygift = $this->Gift_model->getgift($id);
        if ($mygift) {
            $this->response([
                'status' => true,
                'data' => $mygift,
                'message' => 'Produk ditemukan'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $mygift,
                'message' => 'Produk tidak ditemukan'
            ], 404);
        }
    }

    function index_post()
    {
        $idUser = $this->validation->validationToken()->id_user;
        $idMygift = $this->post('id_gift');

        $hadiah = $this->Gift_model->cekIdGiftD($idMygift);

        $data = [
            'id_user'     => $idUser,
            'id_hadiah'   => $hadiah->id_gift,
            'nama_hadiah' => $hadiah->name_gift,
            'point'       => $hadiah->point_gift
        ];

        if ($this->Transaksi_model->cekidPointUser($idUser) == 0) {
            $this->response([
                'status' => false,
                'message' => 'Data Hadiah gagal dimasukkan'
            ], 404);
        } else {
            $dataP = $this->Transaksi_model->cekPoint($idUser);
            $point = $dataP['point'];
            if ($point <= $data['point']) {
                $this->response([
                    'status' => false,
                    'message' => 'Point tidak cukup'
                ], 404);
            } else {
                $pointT = $point - $data['point'];
                if ($this->Mygift_model->ubahPoint($idUser, $pointT) == true) {
                    $a = $this->Mygift_model->tambahTransaksi($data);
                    $this->response([
                        'status' => true,
                        'pointSisa' => $pointT,
                        'message' => 'Hadiah dapat ditukarkan'
                    ], 200);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Hadiah tidak dapat ditukarkan'
                    ], 404);
                }
            }
        }
    }
}
