<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Point extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
    }

    function index_get()
    {
        $idUser = $this->validation->validationToken()->id_user;

        $point = $this->Point_model->getPoint($idUser);
        if ($point) {
            $this->response([
                'status' => true,
                'data' => $point,
                'message' => 'Produk ditemukan'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $point,
                'message' => 'Produk tidak ditemukan'
            ], 404);
        }
    }
}
