<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class TransaksiProduk extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
    }

    function index_get()
    {
        $id = $this->get('id_transaksi');
        $idUser = $this->validation->validationToken()->id_user;
        $status = 0;

        $transaksi = $this->Transaksi_model->getTransaksi($id, $idUser, $status);
        if ($transaksi) {
            $this->response([
                'status' => true,
                'data' => $transaksi,
                'message' => 'Produk ditemukan'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $transaksi,
                'message' => 'Produk tidak ditemukan'
            ], 404);
        }
    }

    function index_post()
    {
        $idUser = $this->validation->validationToken()->id_user;
        $idProduct = $this->post('id_product');

        $produk = $this->Product_model->cekIdProdukD($idProduct);

        $data['id_user'] = $idUser;
        $data['id_product'] = $produk->id_product;
        $data['name_product'] = $produk->name_product;
        $data['price_product'] = $produk->price_product;
        $data['qty_product'] = $this->post('qty_product');
        $data['harga_total'] = $data['price_product'] * $data['qty_product'];
        $data['status'] = 0;

        $this->form_validation->set_rules('qty_product', 'QTY Product', 'required|numeric');

        if ($this->form_validation->run() == false) {
            if (form_error('qty_product')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('qty_product')
                ], 404);
            }
        }

        if ($this->Transaksi_model->tambahTransaksi($data) == true) {
            $this->response([
                'status' => true,
                'point' => $data,
                'message' => 'Produk berhasil ditambahkan'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data transaksi gagal dimasukkan'
            ], 404);
        }
    }

    function checkout_post()
    {
        $idUser = $this->validation->validationToken()->id_user;

        if ($this->Transaksi_model->cekTransaksi($idUser) == true) {
        } else {
            $this->response([
                'status' => false,
                'message' => 'Tidak ada barang di keranjang'
            ], 404);
        }

        if ($this->Transaksi_model->checkout($idUser) == true) {
            if ($this->Transaksi_model->cekidPointUser($idUser) == 0) {
                $dataP = [
                    'id_user' => $idUser,
                    'point' => 5
                ];

                if ($this->Transaksi_model->tambahPoint($dataP) == true) {
                    $this->response([
                        'status' => true,
                        'data' => $dataP,
                        'message' => 'Transaksi masuk, selamat anda mendapat 5 point pertama'
                    ], 200);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data transaksi gagal dimasukkan'
                    ], 404);
                }
            } else {
                $dataP = $this->Transaksi_model->cekPoint($idUser);
                $point = $dataP['point'];
                $pointT = $point + 5;

                if ($this->Transaksi_model->ubahPoint($idUser, $pointT) == true) {
                    $this->response([
                        'status' => true,
                        'point' => $pointT,
                        'message' => 'Transaksi berhasil, point anda bertambah 5'
                    ], 200);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data transaksi gagal dimasukkan'
                    ], 404);
                }
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data transaksi gagal dimasukkan'
            ], 404);
        }
    }
}
