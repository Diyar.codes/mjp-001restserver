<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use \Firebase\JWT\JWT;

class Validation
{
    function validationToken()
    {
        $ci = get_instance();
        $token = $ci->input->get_request_header('X-Token');
        $key = "example_key";
        try {
            $decoded = JWT::decode($token, $key, array('HS256'));
            return $decoded;
        } catch (Exception $e) {
            $ci->response([
                'status'    => false,
                'message'   => 'Token Belum Terdaftar'
            ], 404);
        }
    }
}
