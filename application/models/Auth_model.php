<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    function insertUser($data)
    {
        $insert = $this->db->insert('tbl_users', $data);
        return $insert;
    }

    function getUserByEmail($email)
    {
        $data = $this->db->get_where('tbl_users', ['email' => $email]);
        return $data->row_array();
    }
}
