<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gift_model extends CI_Model
{
    function getGift($id)
    {
        if ($id == null) {
            $gift = $this->db->get('tbl_gifts')->result();
        } else {
            $this->db->where('id_gift', $id);
            $gift = $this->db->get('tbl_gifts')->row();
        }
        return $gift;
    }

    function insertGift($data)
    {
        $data = $this->db->insert('tbl_gifts', $data);
        return $data;
    }

    function cekIdGift($id)
    {
        $data = $this->db->get_where('tbl_gifts', ['id_gift' => $id]);
        return $data->num_rows();
    }

    function cekIdGiftD($id)
    {
        $data = $this->db->get_where('tbl_gifts', ['id_gift' => $id]);
        return $data->row();
    }

    function deleteGift($id)
    {
        $data = $this->db->delete('tbl_gifts', ['id_gift' => $id]);
        return $data;
    }

    function updateGift($id, $data)
    {
        $this->db->where('id_gift', $id);
        $update = $this->db->update('tbl_gifts', $data);
        return $update;
    }
}
