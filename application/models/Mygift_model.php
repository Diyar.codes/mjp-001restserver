<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mygift_model extends CI_Model
{
    function ubahPoint($idUser, $pointT)
    {
        $this->db->where('id_user', $idUser);
        $data = $this->db->update('tbl_point', ['point' => $pointT]);
        return $data;
    }

    function tambahTransaksi($data)
    {
        $data = $this->db->insert('tbl_transaksihadiah', $data);
        return $data;
    }
}
