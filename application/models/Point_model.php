<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Point_model extends CI_Model
{
    function getPoint($idUser)
    {
        $this->db->where('id_user', $idUser);
        $point = $this->db->get('tbl_point')->row();
        return $point;
    }
}
