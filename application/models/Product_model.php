<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{
    function getProduct($id)
    {
        if ($id == null) {
            $product = $this->db->get('tbl_products')->result();
        } else {
            $this->db->where('id_product', $id);
            $product = $this->db->get('tbl_Products')->row();
        }
        return $product;
    }

    function insertProduct($data)
    {
        $data = $this->db->insert('tbl_products', $data);
        return $data;
    }

    function cekIdProduk($id)
    {
        $data = $this->db->get_where('tbl_products', ['id_product' => $id]);
        return $data->num_rows();
    }

    function cekIdProdukD($id)
    {
        $data = $this->db->get_where('tbl_products', ['id_product' => $id]);
        return $data->row();
    }

    function deleteProduk($id)
    {
        $data = $this->db->delete('tbl_products', ['id_product' => $id]);
        return $data;
    }

    function updateProduk($id, $data)
    {
        $this->db->where('id_product', $id);
        $update = $this->db->update('tbl_products', $data);
        return $update;
    }
}
