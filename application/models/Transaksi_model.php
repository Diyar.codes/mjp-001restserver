<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi_model extends CI_Model
{
    function getTransaksi($id, $idUser, $status)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('status', 0);
        if ($id == null) {
            $transaksi = $this->db->get('tbl_transaksiProduk')->result();
        } else {
            $this->db->where('id_transaksi', $id);
            $transaksi = $this->db->get('tbl_transaksiProduk')->row();
        }
        return $transaksi;
    }

    function tambahTransaksi($data)
    {
        $data = $this->db->insert('tbl_transaksiProduk', $data);
        return $data;
    }

    function cekTransaksi($idUser)
    {
        $data = $this->db->get_where('tbl_transaksiProduk', ['id_user' => $idUser, 'status' => 0]);
        return $data->num_rows();
    }

    function checkout($idUser)
    {
        $this->db->where('id_user', $idUser);
        $data = $this->db->update('tbl_transaksiProduk', ['status' => 1]);
        return $data;
    }

    function cekIdPointUser($idUser)
    {
        $data = $this->db->get_where('tbl_point', ['id_user' => $idUser]);
        return $data->num_rows();
    }

    function tambahPoint($dataP)
    {
        $data = $this->db->insert('tbl_point', $dataP);
        return $data;
    }

    function cekPoint($idUser)
    {
        $data = $this->db->get_where('tbl_point', ['id_user' => $idUser]);
        return $data->row_array();
    }

    function ubahPoint($idUser, $pointT)
    {
        $this->db->where('id_user', $idUser);
        $data = $this->db->update('tbl_point', ['point' => $pointT]);
        return $data;
    }
}
